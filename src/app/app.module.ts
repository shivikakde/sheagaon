import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/dashboard/header/header.component';
import { FooterComponent } from './components/dashboard/footer/footer.component';
import { RegisterComponent } from './components/register/register.component';
import { BhaktparivarComponent } from './components/bhaktparivar/bhaktparivar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { NgxSlideshowAcracodeModule } from 'ngx-slideshow-acracode';
import * as $ from 'jquery';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
    BhaktparivarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSlideshowAcracodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
