import { Component, OnInit } from '@angular/core';
import { NgxSlideshowAcracodeModel } from 'ngx-slideshow-acracode';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  imagesUrl = [
    new NgxSlideshowAcracodeModel(
      '../../../../assets/img/la.jpg', 'Button 1'
    ),
    // tslint:disable-next-line:max-line-length
    new NgxSlideshowAcracodeModel(
      '../../../../assets/img/chicago.jpg', 'Button 2'
    )
  ];
  constructor() { }

  ngOnInit() {
  }

  clicked($event) {
    alert('This is' + $event.url);
  }

}
