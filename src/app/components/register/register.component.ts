import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerform: FormGroup;

  constructor() {

    this.registerform = new FormGroup({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      genderform: new FormControl('', Validators.required),
      // tslint:disable-next-line:max-line-length
      mail: new FormControl('', Validators.compose([Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])),
      mobile: new FormControl('', [Validators.minLength(10), Validators.maxLength(10), Validators.required]),
    });
  }

  ngOnInit() {}

  onRegisterSubmit() {

    console.log(this.registerform.value.firstname);
    console.log(this.registerform.value.lastname);
    console.log(this.registerform.value.mail);
    console.log(this.registerform.value.mobile);
    console.log(this.registerform.value.genderform);

  }
}
