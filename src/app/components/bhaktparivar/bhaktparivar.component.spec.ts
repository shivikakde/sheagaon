import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BhaktparivarComponent } from './bhaktparivar.component';

describe('BhaktparivarComponent', () => {
  let component: BhaktparivarComponent;
  let fixture: ComponentFixture<BhaktparivarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BhaktparivarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BhaktparivarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
